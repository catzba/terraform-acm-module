#-----------------------------------------------------------------------------------------------------
# ACM CERTIFICATE's
#-----------------------------------------------------------------------------------------------------

resource "aws_acm_certificate" "cert" {
  domain_name               = var.domain_name
  validation_method         = "DNS"
  subject_alternative_names = ["*.${var.domain_name}"]

  tags = {
    Name        = "${var.project_name}-${var.environment}-cert"
    Project     = var.project_name
    Environment = var.environment
  }

  lifecycle {
    create_before_destroy = true
  }
}


#-----------------------------------------------------------------------------------------------------
# ROUTE 53 Certificate Validation
#-----------------------------------------------------------------------------------------------------

#REQUIRED: Get info about hosted zone 
data "aws_route53_zone" "my_domain" {
  name = var.route53_zone_name
}

resource "aws_route53_record" "cert_validation_domain" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.my_domain.zone_id
}

#-----------------------------------------------------------------------------------------------------
# ACM Certificate Validation
#-----------------------------------------------------------------------------------------------------

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_validation_domain : record.fqdn]
}



# resource "aws_route53_record" "cert_validation_domain" {
#   name    = aws_acm_certificate.cert.domain_validation_options.resource_record_name
#   type    = aws_acm_certificate.cert.domain_validation_options.resource_record_type
#   zone_id = data.aws_route53_zone.my_domain.zone_id
#   records = [aws_acm_certificate.cert.domain_validation_options.resource_record_value]
#   ttl     = "60"
# }


# resource "aws_acm_certificate_validation" "cert" {
#   certificate_arn         = aws_acm_certificate.cert.arn
#   validation_record_fqdns = [aws_route53_record.cert_validation_domain.fqdn]
# }

